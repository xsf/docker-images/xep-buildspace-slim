FROM debian:stable-slim

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y --no-install-recommends && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends python3 git make xsltproc libxml2-utils && apt-get clean && rm -rf /var/lib/apt/lists/*
